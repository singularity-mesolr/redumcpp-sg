# redlumcpp-sg

Project for valentin resseguier
redumcpp-sg

Goal is to launch ITHACA and REDLUMP on MEso@LR

## How to build
```bash
singularity build redlumcpp-sg.sif redlumcpp-sg.def
```

## How to run
First pull the image then run whatever =>
```bash
singularity pull oras://registry.forgemia.inra.fr/singularity-mesolr/redumcpp-sg/redumcpp-sg:latest
./redumcpp-sg_latest.sif redlum-fullOrderPressure
```

## TODO
- %test part of def file

